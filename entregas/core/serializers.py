from rest_framework import serializers
from .models import Maps


class MapsSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = Maps
        fields = ('id', 'name', 'routes')
