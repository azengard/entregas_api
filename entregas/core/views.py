from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Maps
from .serializers import MapsSerializer
from .helper_route import get_best_route_and_price, validate_request


@api_view(['GET', 'POST'])
def map_list(request):
    """
    List all maps or create a new one.
    """
    if request.method == 'GET':
        maps = Maps.objects.all()
        serializer = MapsSerializer(maps, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = MapsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def map_detail(request, name):
    """
    Get, Update or Delete a specific map
    """
    try:
        maps = Maps.objects.get(name=name.upper())
    except Maps.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == 'GET':
        serializer = MapsSerializer(maps)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = MapsSerializer(maps, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

    elif request.method == 'DELETE':
        maps.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    

@api_view(['POST'])
def map_best_route(request, name):
    """
    Find the best route and cost for a specific map
    """
    try:
        maps = Maps.objects.get(name=name.upper())
    except Maps.DoesNotExist:
        message = f"{name} not found"
        return Response(message, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'POST':
        errors = validate_request(request)
        if errors:
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            data = get_best_route_and_price(maps.routes, request)
            return Response(data, status=status.HTTP_200_OK)
