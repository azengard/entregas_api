from django.conf.urls import url, include
from .views import map_list, map_detail, map_best_route


urlpatterns = [
    url(r'^mapas/$', map_list, name='map_list'),
    url(r'^mapas/(?P<name>[\w]+)$', map_detail, name='map_detail'),
    url(r'^mapas/(?P<name>[\w]+)/route$', map_best_route, name='map_best_route'),    
    url(r'^api-auth/', include('rest_framework.urls'))    
]