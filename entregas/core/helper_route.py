from collections import defaultdict
from heapq import heappop, heappush
from rest_framework import serializers


def convert_map(maps):
    """
    Import a text map with routes and return a plain string.

    :maps: text file with a map 
    :return: routes: str
    """
    try:
        with open(maps, 'r') as my_map:
            routes = my_map.read()
    except FileNotFoundError as ex:
        raise serializers.ValidationError('The route file was not found')

    return routes


def get_best_route_and_price(routes, request):
    """
    Find the best route and price.
    :routes: 
    :request: request object with the data 
    :return: {'route': ['A', 'B']:dict 'cost': 6.25: float} if OK
        or {'route': 'Inf':str, 'cost': 0:float} if ok but don't exist a path
    """
    routes = parse_map(routes)
    start = request.data.get('start', '')
    end = request.data.get('end', '')
    autonomy = request.data.get('autonomy', 0)
    liter = request.data.get('liter', 0)

    distance, route = get_shortest_path(routes, start, end)
    freight_cost = calc_freight_cost(distance, autonomy, liter)

    data = {
        'route': route,
        'cost': freight_cost
    }

    return data

def validate_request(request):
    """
    Validate the type of all data in POST /route
    """
    start = request.data.get('start')
    end = request.data.get('end')
    autonomy = request.data.get('autonomy')
    liter = request.data.get('liter')
    errors = []

    if type(start) is not str:
        errors.append("'start' must be declared as a string")
    if type(end) is not str:
        errors.append("'end' must be declared as a string")
    if type(autonomy) is not float and type(autonomy) is not int:
        errors.append("'autonomy' must be declared as a float or integer")
    if type(liter) is not float and type(liter) is not int:
        errors.append("'liter' must be declared as a float or integer")

    return errors



def parse_map(routes):
    """
    Import a text map with routes and parse it.

    :maps: text file with a map
    :return: [("start": str, "end": str, "wheight": float), ( ... ), ...]
    """
    map_list = []

    try:
        routes = routes.split('\n')
        for route in routes:
            res = route.split()
            res[2] = float(res[2])
            map_list.append(tuple(res))
    except ValueError as ex:
        raise serializers.ValidationError("The route has a invalid format.")

    return map_list


def calc_freight_cost(distance, autonomy, liter_value):
    """
    Calculate the cost of a freight

    :distance: 25:float
    :autonomy: 10:float
    :liter_value: 2.50:float
    :return: 6.25: float
    """

    freight_cost = (distance/autonomy) * liter_value
    return freight_cost


def get_shortest_path(routes, start, end):
    """
    Calculate the shortest path for a directed weighted graph.

    :param start: starting node
    :param end: ending node
    :param routes: [("start": str, "end": str, "wheight": float), ( ... ), ...]
    :return: (cost: int, path_list: tuple) or 'inf': str, if there isn't a path
    """

    grafh = defaultdict(list)
    for p_ini, p_final, distance in routes:
        grafh[p_ini].append((distance, p_final))

    query_list = [(0, start, ())]
    seen = set()

    while query_list:
        (cost, val_locked, path) = heappop(query_list)

        if val_locked not in seen:
            seen.add(val_locked)
            path = (val_locked, path)
            if val_locked == end:
                path = _clean_tuple(path)
                return (cost, path)

            for distance, new_value in grafh.get(val_locked, ()):
                if new_value in seen:
                    continue
                heappush(query_list, (cost + distance, new_value, path))

    return (0, "inf")


def _clean_tuple(path):
    """
    Unpack a tuple and return a reversed tuple
    with the cleaned path in the right order

    :param path: ('D', ('B', ('A', ()))): tuple
    :return: ('A', 'B', 'D'): tuple
    """

    path = str(path)
    path_list = []
    for element in path:
        if element.isalpha():
            path_list.append(element)

    return tuple(path_list[::-1])

