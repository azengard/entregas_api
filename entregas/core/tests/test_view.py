from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from entregas.core.models import Maps


class ViewTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""

        user = User.objects.create(username="test")
        self.client = APIClient()
        self.client.force_authenticate(user=user)

        self.maps_data = {'name': 'SP', 'routes': 'example_data/sp_map.txt'}
        self.response = self.client.post(
            reverse('map_list'),
            self.maps_data,
            format="json")

    def test_map_list_can_create_a_map(self):
        """Test if map_list() has maps creation capability."""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_authorization_is_enforced(self):
        """Test that the api has user authorization."""
        new_client = APIClient()
        response = self.client.get(
            reverse('map_list'),
            format="json"
        )
        res = new_client.get(reverse('map_list'), format="json")
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_can_get_all_maps(self):
        """Test the api can get a list of maps."""
        maps = Maps.objects.all()
        response = self.client.get(reverse('map_list'), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_map_detail_can_get_map(self):
        """Test if map_detail() can get a given map."""
        maps = Maps.objects.get(name='SP')
        response = self.client.get(
            reverse('map_detail', kwargs={'name': maps.name}),
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, maps)

    def test_map_detail_can_update_map(self):
        """Test if map_detail() can update a given map."""
        maps = Maps.objects.get()
        change_map = {'name': 'SC', 'routes': 'example_data/sc_map.txt'}
        res = self.client.put(
            reverse('map_detail', kwargs={'name': maps.name}),
            change_map, format='json'
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_map_detail_can_delete_map(self):
        """Test if map_detail() can delete a map."""
        maps = Maps.objects.get()
        response = self.client.delete(
            reverse('map_detail', kwargs={'name': maps.name}),            
            format='json', follow=True
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_map_best_route_can_return_route_and_price(self):
        """Test if map_best_route() can return the best route and cost
        for a determinade freigth."""
        self.sample = { "route": ("A", "B", "D"), "cost": 6.25 }
        self.data = {
            'start': 'A',
            'end': 'D',
            'autonomy': 10,
            'liter': 2.5,
        }
        maps = Maps.objects.get(name='SP')
        response = self.client.post(
            reverse('map_best_route', kwargs={'name': maps.name}),
            self.data,         
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.sample)