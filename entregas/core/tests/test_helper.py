from django.test import TestCase
from django.core.urlresolvers import reverse
from rest_framework.test import APIRequestFactory
from entregas.core.helper_route import (convert_map, parse_map,
    calc_freight_cost, get_shortest_path, get_best_route_and_price,
    validate_request)


class HelperTestCase(TestCase):

    def setUp(self):
        self.valid_data = {
            'start': 'A',
            'end': 'D',
            'autonomy': 10,
            'liter': 2.5
        }
        self.map_routes = 'example_data/sp_map.txt'

        self.converted_map = convert_map(self.map_routes)
        self.parsed_map = parse_map(self.converted_map)

        self.factory = APIRequestFactory()
        self.request = self.factory.post(
            reverse('map_best_route', kwargs={'name': 'SP'}), 
            format='json'
        )
        self.request.data = self.valid_data


    def test_convert_map_is_string(self):
        """
        Pass a file path to convert_map() and verify if returns a string.
        """
        self.assertEqual(type(self.converted_map), str)

    def test_parse_map(self):
        """
        Pass a route to parse_map() and verify if returns object was
        parsed correctly.
        """
        self.sample = [
            ('A', 'B', 10.0), ('A', 'C', 20.0), 
            ('B', 'D', 15.0), ('C', 'D', 30.0), 
            ('B', 'E', 50.0), ('D', 'E', 30.0)
        ]

        self.assertEqual(self.parsed_map, self.sample)

    def test_get_shortest_path(self):
        """
        Pass routes, start, end and verify if returns object is correct.
        """
        self.sample = (25.0, ('A', 'B', 'D'))
        self.start = 'A'
        self.end = 'D'

        self.short_path = get_shortest_path(self.parsed_map, self.start, self.end)
        self.assertEqual(self.short_path, self.sample)

    def test_calc_freight_cost(self):
        """
        Pass a distance, autonomy, liter_value to calc_freight_cost and 
        verify if returns object is correct.
        """
        self.distance = 25
        self.autonomy = 10
        self.liter_value = 2.5
        self.sample = 6.25

        self.freight_cost = calc_freight_cost(self.distance, self.autonomy, self.liter_value)
        self.assertEqual(self.freight_cost, self.sample)

    def test_get_best_route_and_price(self):
        """
        Pass a valid route and a request object to get_best_route_and_price()
        and verify if returns object are correct.
        """
        self.sample = {'route': ('A', 'B', 'D'), 'cost': 6.25}
        self.route = "A B 10\nA C 20\nB D 15\nC D 30\nB E 50\nD E 30"
        self.res = get_best_route_and_price(self.route ,self.request)

        self.assertEqual(self.res, self.sample)

    def test_validate_request_valid(self):
        """
        Send a valid request and verify if return no errors
        """
        self.res = validate_request(self.request)
        self.assertFalse(self.res)
        

    def test_validate_request_invalid(self):
        """
        Send a invalid request and verify if return erros
        """
        self.invalid_data = {
            'start': 'A',
            'end': 'D',
            'autonomy': '10',
            'liter': 2.5
        }
        self.request.data = self.invalid_data

        self.sample = ["'autonomy' must be declared as a float or integer"]
        self.res = validate_request(self.request)
        self.assertEqual(self.res, self.sample)
