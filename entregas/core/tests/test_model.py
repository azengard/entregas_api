from django.test import TestCase
from entregas.core.models import Maps


class ModelTestCase(TestCase):
    """This class defines the test suite for the Maps model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.map_name = 'SP'
        self.map_routes = 'example_data/sp_map.txt'
        self.maps = Maps(name=self.map_name, routes=self.map_routes)

    def test_model_can_create_a_map(self):
        """Test the Maps model can create a new map."""
        old_count = Maps.objects.count()
        self.maps.save()
        new_count = Maps.objects.count()
        self.assertNotEqual(old_count, new_count)
