from django.db import models
from .helper_route import convert_map


class Maps(models.Model):
    name = models.CharField(max_length=30, unique=True)
    routes = models.TextField()

    class Meta:
        verbose_name = 'Mapa'
        verbose_name_plural = 'Mapas'
        
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        """
        Save a text plain parsed map with the routes in the DB
        """
        self.name = self.name.upper()
        self.routes = convert_map(self.routes)
        super(Maps, self).save(*args, **kwargs)