# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Application to solve the minimal cost freight.
It find the best route and return the route with the freight cost.

I used the dijkstra algorithm to find the best route, he works only when the wheights (in this case the distance
between 2 points) is positive, and can return all intermediate steps.

It's implemented with the python heapq function for controling the list with the best path (so far), this function
has a good performance and its 'pop' function simple solve the step of remove the minor value of the list for calculate
the next round of minor values. 

* Version
1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

1. Clone the repository.
2. Create a virtualenv with Python 3.6
3. Activate the virtualenv.
4. Install the dependencies.
5. Configure the instance with the .env
6. Run the tests.

* Configuration

```console
git clone git@bitbucket.org:azengard/entregas_api.git
  or
git clone https://azengard@bitbucket.org/azengard/entregas_api.git
cd entregas_api
python -m venv .entregas
source .entregas/bin/activate
```

* Dependencies

```console
pip install -r requirements.txt
cp contrib/.env_sample .env
echo SECRET_KEY=`python contrib/secret_gen.py` >> .env
```

* Database configuration

```console
python manage.py migrate
```

* How to run tests

```console
python manage.py test
```

* Create a user

```console
python manage.py createsuperuser
```

* Run instructions

```console
python manage.py runserver
```

### How call the API using HttPie ###


** Create a new map **

*name* A unique name for this map.

*routes* send a path to a .txt file with the route format like this example:

```
A B 10
A C 20
B C 15
```

```console
http -a <username>:<password> POST localhost:8000/mapas/ name="sp" routes="example_data/sp_map.txt"
```

You will get this return:

```console
HTTP/1.0 201 Created
...
Content-Type: application/json
...
{
    "id": 1, 
    "name": "SP", 
    "routes": "A B 10\nA C 20\nB D 15\nC D 30\nB E 50\nD E 30"
}
```


** List all maps **

```console
http -a <username>:<password> GET localhost:8000/mapas/
```

You will get this return:

```console
HTTP/1.0 200 OK
...
Content-Type: application/json
...
[
    {
        "id": 1, 
        "name": "SP", 
        "routes": "A B 10\nA C 20\nB D 15\nC D 30\nB E 50\nD E 30"
    }, 
    {
        ...
    },
    ...
]
```


** Get a map **

```console
http -a <username>:<password> GET localhost:8000/mapas/sp
```

You will get this return:

```console
HTTP/1.0 200 OK
...
Content-Type: application/json
...
{
    "id": 1, 
    "name": "SP", 
    "routes": "A B 10\nA C 20\nB D 15\nC D 30\nB E 50\nD E 30"
}
```


** Find the price(cost) and the shortest route **

*name* The name of the map you will use

*start* The start point of the route

*end* The ending point of the route

*autonomy* The autonomy of the truck

*liter* The cost of the liter

```console
http -a <username>:<password> localhost:8000/mapas/<name>/route start=A end=D autonomy:=10 liter:=2.5
```

You will get this return:

```console
HTTP/1.0 200 OK
...
Content-Type: application/json
...
{
    "cost": 6.25, 
    "route": [
        "A", 
        "B", 
        "D"
    ]
}
```

If there is no path available for this freight the return will be this:

```console
HTTP/1.0 200 OK
...
Content-Type: application/json
...
{
    "cost": 0.0, 
    "route": "inf"
}
```


** Update a map **

```console
http -a <username>:<password> PUT localhost:8000/mapas/<name> name=sc routes=example_data/sc_map.txt
```

You will get this return:

```console
HTTP/1.0 200 OK
...
Content-Type: application/json
...
{
    "id": 1, 
    "name": "SC", 
    "routes": "A B 50\nA C 20\nA F 30\nB D 30\nB C 20\nC D 10\nC F 20\nB E 20\nD E 30\n"
}
```


** Delete a map **

```console
http -a <username>:<password> DELETE localhost:8000/mapas/<name>
```

You will get this return:

```console
HTTP/1.0 204 No Content
...
Content-Type: application/json
...
```


### Who do I talk to? ###

* Repo owner or admin ** Carlos Henrique **

* Other community or team contact